<?php
date_default_timezone_set("Europe/Madrid");
class DB_mysql {
	/* variables de conexión */
	var $BaseDatos;
	var $Servidor;
	var $Usuario;
	var $Clave;

	/* identificador de conexión y consulta */
	var $Conexion_ID = 0;
	var $Consulta_ID = 0;

	/* número de error y texto error */
	public $Errno = 0;
	public $Error = "";

	function cambiarfecha($fecha){
			return implode('-', array_reverse(explode('-', $fecha)));
	}

	/* Método Constructor: Cada vez que creemos una variable de esta clase, se ejecutará esta función */
	function DB_mysql ($bd = "db499056097", $host = "db499056097.db.1and1.com", $user = "dbo499056097", $pass = "Oficina3652012") {
		$this->BaseDatos = $bd;
		$this->Servidor = $host;
		$this->Usuario = $user;
		$this->Clave = $pass;
	}

	/*Conexión a la base de datos*/
	function conectar($bd="", $host="", $user="", $pass=""){
		if ($bd != "") $this->BaseDatos = $bd;
		if ($host != "") $this->Servidor = $host;
		if ($user != "") $this->Usuario = $user;
		if ($pass != "") $this->Clave = $pass;

		// Conectamos al servidor
		$this->Conexion_ID = mysqli_connect($this->Servidor, $this->Usuario, $this->Clave, $this->BaseDatos);
		if (!$this->Conexion_ID) {
			$this->Error = "Ha fallado la conexión.";
			return 0;
		}

		mysqli_query ($this->Conexion_ID, "SET NAMES 'utf8'");

		/* Si hemos tenido éxito conectando devuelve el identificador de la conexión, sino devuelve 0 */
		return $this->Conexion_ID;
	}
	
	/* Vaciar consulta realizada */
	function VaciarConsulta(){
		mysqli_free_result($this->Consulta_ID);
	}
	
	/* Desconecta la Base de Datos */
	function Desconectar(){
		mysqli_close($this->Conexion_ID);
	}
	
	function SQLSeguro($valor){
		return mysqli_real_escape_string($this->Conexion_ID, $valor);
	}
	/* Ejecuta un consulta */
	function consulta($sql = ""){
		$this->Error = "";
		if ($sql == "") {
			$this->Error = "No ha especificado una consulta SQL";
			return 0;
		}
		//ejecutamos la consulta
		$this->Consulta_ID = @mysqli_query($this->Conexion_ID, $sql);
		if (!$this->Consulta_ID) {
			$this->Error = mysqli_error($this->Conexion_ID);
			return -1;
		}
		/* Si hemos tenido éxito en la consulta devuelve el identificador de la conexión, sino devuelve 0 */
		return $this->Consulta_ID;
	}

	/* Devuelve el ultimo ID creado */
	function ultimoID(){
		if ($this->Consulta_ID==0){
			return 0;
		}else{
			return mysqli_insert_id($this->Consulta_ID);
		}
	}

	/* Devuelve el número de campos de una consulta */
	function numcampos() {
		if ($this->Consulta_ID==0){
			return 0;
		}else{
			return mysqli_field_count($this->Consulta_ID);
		}
	}

	/* Devuelve el número de registros de una consulta */
	function numregistros(){
		if ($this->Consulta_ID==0){
			return 0;
		}else{
			return mysqli_num_rows($this->Consulta_ID);
		}
	}

	/* Devuelve el nombre de un campo de una consulta */
	function nombrecampo($numcampo) {
		if ($this->Consulta_ID==0){
			return 0;
		}else{
			$info_campo= mysqli_fetch_field_direct($this->Consulta_ID, $numcampo);
			return $info_campo->name;
		}
	}

	function tipocampo($numcampo){
		if ($this->Consulta_ID==0){
			return 0;
		}else{
			$info_campo= mysqli_fetch_field_direct($this->Consulta_ID, $numcampo);
			return $info_campo->type;
		}
	}

	function longitudcampo($numcampo){
		if ($this->Consulta_ID==0){
			return 0;
		}else{
			$info_campo= mysqli_fetch_field_direct($this->Consulta_ID, $numcampo);
			return $info_campo->max_length;
		}
	}

	function DatoCampo($Dato){
		if ($this->Consulta_ID==0){
			return 0;
		}else{
			$row =mysqli_fetch_array($this->Consulta_ID, MYSQLI_ASSOC);
			return $row[$Dato];
		}
	}
}

//fin de la Clse DB_mysql

?>